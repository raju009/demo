FROM node:12-alpine
### Install build toolchain, install node deps and compile native add-ons and compile typescript to js code
RUN apk add --no-cache python make g++
WORKDIR /app

RUN  echo //registry.npmjs.org/:_authToken=65811ec3-c3c4-4f2c-9507-98b74adfaa8d > .npmrc
COPY . .
RUN npm install && npm update
RUN npm run build
RUN rm -rf src/

## Can add sections for tests and coverage checks

FROM node:alpine as app
## Copy built node modules and binaries without including the toolchain
WORKDIR /app
RUN npm install pm2 -g
COPY --from=builder /app/ .
# COPY --from=builder /app/dist .
CMD ["pm2-runtime", "start", "dist/index.js", "-i", "max"]
